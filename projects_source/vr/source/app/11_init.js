var disable_control = false;
var help_video_is_showed = false;
var is_tablet;
var is_mobile;

$(document).ready(function () {
    is_tablet = device.tablet();
    is_mobile = device.mobile();

    // прелоадер
    //if ($.cookie('visited') == undefined) {
    //    preloader_animation();
    //}
    //else {
    $('.preloader').addClass('hidden-block');
    $('.page-wrap').removeClass('hidden');
    //}

    // СЛАЙДЕРЫ
    // основной полноэкранный слайдер
    if ($('.fullpage').length) {
        init_fullscreen_slider();
    }
    // внутренний слайдер в табах
    if ($('.inner-slider').length) {
        init_inner_slider();
    }

    if ($('.scrollbar').length) {
        init_scrollbar();
    }

    // Скрыть декоративный слайдер
    var decor_block = $('.js--hide-overlay');

    decor_block.on('mousewheel', function (e) {
        if (e.deltaY < 0) {
            hide_overlay();
        }
        e.preventDefault();
    });

    if (is_tablet || is_mobile) {
        decor_block.swipe({
            swipe: function (event, direction, distance, duration, fingerCount, fingerData) {
                if (direction == 'up' && !disable_control) {
                    disable_control = true;
                    hide_overlay();
                    activate_control(1000);
                }
                if (direction == null) {
                    event.target.click();
                }
            },
            threshold: 10
        });
    }

    // показать декоративный слайдер
    var open_block = $('.js--show-overlay');

    open_block.on('mousewheel', function (e) {
        if (e.deltaY > 0) {
            show_overlay();
        }
        e.preventDefault();
    });

    if (is_tablet || is_mobile) {
        open_block.swipe({
            swipe: function (event, direction, distance, duration, fingerCount, fingerData) {
                if (direction == 'down') {
                    show_overlay();
                }
                if (direction == null) {
                    event.target.click();
                }
            },
            threshold: 10
        });
    }

    // попапы
    $(document).on('click', '.js--open-block', function () {
        $($(this).attr('href')).addClass('open');
        $('body').addClass('fixed');
        return false;
    });

    $(document).on('click', '.js--close-block', function () {
        $($(this).attr('href')).removeClass('open');
        $('body').removeClass('fixed');
        return false;
    });

    // PRICE
    // смена оборудования в форме бронирования
    $(document).on('click', '.js--set-eq', function () {
        $('.inp-eq').val($(this).data('eq'));
    });

    // LIFE
    if ($('.calendar-wrap').length) {
        init_calendar();
    }

    // слайдер фоновых изображений в Life
    if ($('.life-slider').length) {
        init_life_slider();
    }

    // КОННТАКТЫ
    if ($('#map').length) {
        init_map();
    }

    $(document).on('click', '.js--toggle-panel', function () {
        $('.contacts-panel').toggleClass('open');
        return false;
    });

    // ГЛАВНАЯ
    // форма бронирования
    if ($('.inp-phone').length) {
        set_mask();
    }
    $('.index-booking-info .required').on('blur', function () {
        check_input_data();
        refresh_booking_data();
    });

    // выпадающий список
    if ($('.js--select-styled').length) {
        init_style_select();
    }

    // перекрытие инпута с датй на мобильном
    $('.inp-date-mobile-overlay').on('click', function () {
        $('.inp-date').trigger('focus');
    });

    // слайдер с участниками
    if ($('.slider-vertical').length) {
        init_vertical_slider();
    }

    // выбор времени посещения
    $(document).on('click', '.js--change-time', function () {
        check_chioced_time();
        count_time_price();
    });

    // выбор оборудования
    $(document).on('change', '.form-booking .js--change-eq', function () {
        refresh_booking_data();
    });

    // применить скидку
    $('.inp-discount').blur(function () {
        check_discount($(this));
    });

    // переключене табов в форме бронирования
    $(document).on('click', '.js--next', function () {
        switch_booking_step('next');
        return false;
    });

    $(document).on('click', '.js--prev', function () {
        switch_booking_step('prev');
        return false;
    });

    // ЦЕНА
    $(document).on('click', '.js--change-eq', function () {
        $('.inp-eq').val($(this).data('eq'));
    });

    // SPACE
    if ($('.space-content-grid').length) {
        init_space_gallery();
    }

    // GAMES
    // слайдер с играми
    if ($('.games-slider').length) {
        games_html = $('.games-slider-container').html();
        check_games_view();
        init_filter();

        $(window).resize(function () {
            check_games_view();
        });

        window.addEventListener("orientationchange", function () {
            check_games_view();
        }, false);
    }

    // наведение на игру
    $(document).on('mouseenter', '.games-slider--el', function () {
        if ($(window).width() > 1024) {
            var container = $(this).closest('.games-slider--el');
            container.find('.slide-wrap').stop().slideDown(200);
            container.find('.game-content--btns').stop().fadeIn(200);
        }

    });
    $(document).on('mouseleave', '.games-slider--el', function () {
        if (!(device.mobile() == false && device.tablet() == false && $(window).width() > 1024)) {
            return false;
        }
        if ($(window).width() > 1024) {
            var container = $(this).closest('.games-slider--el');
            container.find('.slide-wrap').stop().slideUp(200);
            container.find('.game-content--btns').stop().fadeOut(200);
        }
    });

    // форма поиска
    $('.js--open-search').on('click', function () {
        $('.search-form').toggleClass('open');
        return false;
    });

    $(document).on('click', '.js--search-submit', function () {
        search_games();
        return false;
    });

    $(document).on('submit', '.search-form', function () {
        $('.inp-search').blur();
        search_games();

        return false;
    });

    // применить фильтр
    $(document).on('click', '.js--set-filter', function () {
        set_filter('true');
        return false;
    });

    // показать информацию об игре
    $(document).on('click', '.js--game-info', function () {
        show_game_info($(this));
        return false;
    });

    // планшеты/мбильные показать текст полностью
    $(document).on('click', '.js--toggle-game-about', function () {
        var btn = $(this);
        $('.mobile-text').fadeToggle(300);
        btn.toggleClass('hide');
        if (btn.hasClass('hide')) {
            btn.text(btn.data('close'))
        } else {
            btn.text(btn.data('open'))
        }
        return false;
    });

    $('ul.tabs--caption').on('click', 'li:not(.active)', function (i) {
        $(this).addClass('active').siblings().removeClass('active');
        $(this).closest('.tabs').find('div.tabs--content').removeClass('active').eq($(this).index()).addClass('active');
    });
    $('ul.tabs-inner--caption').on('click', 'li:not(.active)', function () {
        $(this).addClass('active').siblings().removeClass('active').closest('.tabs-inner').find('.time-list .tabs-inner--content').removeClass('active').eq($(this).index()).addClass('active');
    });

    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    $('.inp').on('focus', function(){
        $(this).removeClass('error')
    });

    $('.js--form-submit').on('click', function () {
        var btn = $(this);
        var form = btn.closest('.main-form');
        var errors = false;

        $(form).find('.inp.required').each(function () {
            var inp = $(this);
            var val = inp.prop('value');
            if (val == '' || val == undefined) {
                inp.addClass('error');
                errors = true;
            }
            else {
                if (inp.hasClass('inp-mail')) {

                    if (validateEmail(val) == false) {
                        inp.addClass('error');
                        errors = true;
                    }
                }
            }
        });

        if (errors == false) {
            var button_value = btn.text();
            btn.text('Подождите...');

            var method = form.attr('method');
            var action = form.attr('action');
            var data = form.serialize();

            $.ajax({
                type: method,
                url: action,
                data: data,
                success: function (data) {
                    form.find('.inp').each(function () {
                        $(this).prop('value', '')
                    });
                    if(form.hasClass('form-booking')){
                        var parse_data = jQuery.parseJSON(data);
                        window.location = parse_data.url;
                    }
                    else{
                        form.closest('section').removeClass('open');
                        $('.section-sended').addClass('open');
                    }
                },
                error: function (data) {
                    btn.text('Ошибка');
                    setTimeout(function () {
                        $(form).find('.js-form-submit').text(button_value);
                    }, 2000);
                }
            });
        }

        return false;
    });
});