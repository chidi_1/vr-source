function init_vertical_slider() {
    //var current = $('.inp-members');
    var min = $('.slider-vertical').data('min');
    var max = $('.slider-vertical').data('max');

    var slider = document.getElementById('slider');

    noUiSlider.create(slider, {
        connect: true,
        orientation: 'vertical',
        step: 1,
        direction: 'rtl',
        behaviour: 'tap-drag',
        start: [1],
        range: {
            'min': min,
            'max': max
        }
    });

    slider.noUiSlider.on('update.one', function(values){
        var val = Number(values[0]);
        val = val.toFixed(0);
        $('.inp-members').val(val);
        $('.members-list li').removeClass('active').eq(val - 1).addClass('active');
        count_time_price();
        refresh_booking_data();
    });

    $(document).on('click', '.js--change-members', function () {
        var link = $(this);
        link.addClass('active').siblings().removeClass('active');
        slider.noUiSlider.set([link.text()]);
        $('.inp-members').val(link.text());

        count_time_price();
    });
}

function init_style_select() {
    $('.js--select-styled').styler({
        onSelectClosed: function () {
            if ($(this).closest('form').hasClass('form-booking')) {
                var option = $(this).find('.jq-selectbox__dropdown ul li.selected');
            }
        }
    });
}

function set_mask() {
    $('.inp-phone').mask("+7 (999) 999 99 99", {
        completed: function () {
            check_input_data();
        }
    });
}

// переключение табов в форме бронирования
function switch_booking_step(direction) {
    var index = $('.form-booking .tabs--caption li.active').index();
    var index_text = index + 1;
    direction == 'next' ? index++ : index--;
    direction == 'next' ? index_text++ : index_text--;

    $('.step').html(index_text + " <i>—</i> 5");
    $('.form-booking .tabs--caption li.active').removeClass('active');
    $('.form-booking .tabs--caption li').eq(index).removeClass('disabled').addClass('active');
    $('.form-booking .tabs--content').removeClass('active').eq(index).addClass('active');
    check_booking_btns();
}

// проверить состояние кнопок
function check_booking_btns() {
    var index = $('.form-booking .tabs--caption li.active').index();

    if (index == 0) {
        $('.js--prev').addClass('hidden-block');
        $('.index-booking-btns').addClass('one-btn');
    }
    else {
        if (index == 4) {
            $('.js--next').addClass('hidden-block');
            $('.js--form-submit').removeClass('hidden-block');
        }
        else {
            $('.index-booking-btns').removeClass('one-btn');
            $('.js--prev').removeClass('hidden-block');
            $('.js--next').removeClass('hidden-block disabled');
            $('.js--form-submit').addClass('hidden-block');

            if (index == 2) {
                if (help_video_is_showed == false) {
                    help_video_is_showed = true;
                    $('.params-popup').addClass('open');
                }

                if ($('.js--change-time:checked').length != 0) {
                    $('.js--next').removeClass('disabled');
                }
                else {
                    $('.js--next').addClass('disabled');
                }

                check_chioced_time();
            }

            if (index == 3) {
                check_input_data();
            }
        }
    }
}

// проверка выбранного времени
function check_chioced_time() {
    if ($('.js--change-time:checked').length != 0) {
        $('.js--next').removeClass('disabled');
    }
    else {
        $('.js--next').addClass('disabled');
    }
}

// проверка введенных данных
function check_input_data() {
    $('.index-booking-info .inp.required').each(function () {
        var error = false;
        if ($(this).val() == '' || $(this).val() == 'undefined') {
            error = 'true';
        }

        if (error == 'true') {
            $('.js--next').addClass('disabled');
        }
        else {
            $('.js--next').removeClass('disabled');
        }
    })
}

// стоимость посещения при выборе времени
function count_time_price() {
    var members = Number($('.inp-members').val());
    var price = 0;
    $('.time-list input:checked').each(function () {
        price = price + Number($(this).data('price')) * members;
    });
    var time = Number($('.time-list input:checked').length);
    $('.time-total-min').text(time * 30 + ' мин');
    $('.time-total-val').text(price + ' руб');

    refresh_booking_data()
}

// проверка скидки
function check_discount(input) {
    var action = input.data('action');
    var method = input.data('method');
    var data = input.val();

    $.ajax({
        url: action,
        method: method,
        data: {data: data},
        success: function (data) {
            var parse_data = jQuery.parseJSON(data);
            if (parse_data.error == false) {
                input.addClass('error')
            }
            else {
                $('.val-discount strong').text(parse_data.value);
                refresh_booking_data();
            }
        }
    })
}


// финальные данные
function refresh_booking_data() {
    var form = $('.form-booking');
    var time = Number($('.time-list input:checked').length);
    var price = time * 500 * Number($('.params-members input').val());
    var members = form.find('.params-members input').val();

    if (members == 2 || members == 3 || members == 4) {
        var members_text = ' человека';
    }
    else {
        var members_text = ' человек';
    }

    // имя
    if (form.find('.inp-name').val() != '' || form.find('.inp-name').val() != 'undefined') {
        $('.index-booking-finish h2 .name').text(form.find('.inp-name').val() + ',');
    }
    // телефон
    $('.index-booking-finish .val-phone strong').text(form.find('.inp-phone').val());
    // дата
    $('.index-booking-finish .val-date strong').text(form.find('.inp-date').val());
    // время
    $('.index-booking-finish .val-time strong').text($('.js--change-time:checked:first-child').val());
    // Оборудование
    $('.index-booking-finish .val-eq strong').text(form.find('.js--change-eq:checked').val());
    // игроки
    $('.index-booking-finish .val-members strong').text(members + members_text);
    // длительность
    $('.index-booking-finish .val-durability strong').text(time * 30 + ' мин');
    // цена
    $('.index-booking-finish .val-price strong').text(price + ' руб');
    // итоговая цена
    if (price != 0) {
        $('.index-booking-finish .val-total strong').text(price - Number($('.inp-discount').data('discount')) + ' руб');
    }
    else {
        $('.index-booking-finish .val-total strong').text('0 руб');
    }
}
