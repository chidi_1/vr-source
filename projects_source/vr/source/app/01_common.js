// прелоадер
function preloader_animation() {
    $('.preloader-square-top').addClass('open');
    $('.preloader-square-bottom').addClass('open');
    $.cookie('visited', 'true', {expires: 10});

    setTimeout(function () {
        $('.preloader-letter-top').addClass('open visible');
        $('.preloader-letter-bottom').addClass('open visible');
    }, 2000);

    setTimeout(function () {
        $('.preloader-square-top').addClass('red');
        $('.preloader-square-bottom').addClass('red');
        $('.preloader-letter-top').addClass('red');
        $('.preloader-letter-bottom').addClass('red');
    }, 4000);

    setTimeout(function () {
        $('.preloader').addClass('hidden-block');
        $('.page-wrap').removeClass('hidden');
    }, 6000);
}

// постраничный слайдер
function init_fullscreen_slider() {
    var slider = $('.fullpage');
    var anchors_content = [];

    slider.find('.section').each(function () {
        anchors_content.push(String($(this).index()));
    });

    slider.fullpage({
        css3: true,
        scrollingSpeed: 500,
        lockAnchors: true,
        anchors: anchors_content,
        navigation: true,
        navigationPosition: 'right',
        navigationTooltips: anchors_content,
        showActiveTooltip: true,
        afterRender: function () {
            if ($('.space-slider').length) {
                $('.space-slider').owlCarousel({
                    margin: 0,
                    loop: true,
                    nav: true,
                    dots: false,
                    autoplay: true,
                    navText: [,],
                    autoplayHoverPause: false,
                    autoplayTimeout: 3000,
                    items: 1
                });

            }
        }
    });
}

var month_names_declension = ["января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря"];

// инициализация выбора даты
function init_calendar() {
    var cur_date = $(new Date())[0];
    var input = $('.js--inp-date');
    var date_min;
    var date_max;

    !input.data('min') ? date_min = null : date_min = $(new Date())[0];
    !input.data('min') ? date_max = null : date_max = (date_min.getDate() + Number(input.data('max')));


    set_calendar_data(cur_date);

    if ($('.js--inp-date').length) {
        $('.js--inp-date').datepicker({
            minDate: date_min,
            maxDate: date_max,
            dateFormat: 'dd.mm.yy',
            monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь','Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            monthNamesShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
            dayNames: ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],
            dayNamesShort: ['вск', 'пнд', 'втр', 'срд', 'чтв', 'птн', 'сбт'],
            dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
            onSelect: function () {
                var start = $('.js--inp-date').val(), end = $('.js--inp-date').val();
                var stArr = start.split('.'), endArr = end.split('.');
                var new_date = new Date(stArr[2], parseInt(stArr[1]) - 1, parseInt(stArr[0]));
                var text_date = new_date.getDate() + ' ' + month_names_declension[new_date.getMonth()] + ' ' + new_date.getFullYear();
                $('.js--inp-date').val(text_date)
                set_calendar_data(new_date);
                get_date_data(input);
            }
        });
    }
}

// установка даты
function set_calendar_data(date) {
    var prev_date = new Date(date.getFullYear(), date.getMonth(), date.getDate() - 1);
    var next_date = new Date(date.getFullYear(), date.getMonth(), date.getDate() + 1);
    var prev_month = new Date(date.getFullYear(), date.getMonth() - 1, date.getDate());
    var next_month = new Date(date.getFullYear(), date.getMonth() + 1, date.getDate());
    var prev_year = new Date(date.getFullYear() - 1, date.getMonth(), date.getDate());
    var next_year = new Date(date.getFullYear() + 1, date.getMonth(), date.getDate());

    $('.calendar-date .prev').text(prev_date.getDate());
    $('.calendar-date .current').text(date.getDate());
    $('.calendar-date .next').text(next_date.getDate());

    $('.calendar-month .prev').text(month_names[prev_month.getMonth()]);
    $('.calendar-month .current').text(month_names[date.getMonth()]);
    $('.calendar-month .next').text(month_names[next_month.getMonth()]);

    $('.calendar-year .prev').text(prev_year.getFullYear());
    $('.calendar-year .current').text(date.getFullYear());
    $('.calendar-year .next').text(next_year.getFullYear());
}

var month_names = ["январь", "февраль", "март", "апрель", "май", "июнь", "июль", "август", "сентябрь", "октябрь", "ноябрь", "декабрь"];

// табы
//$(document).on('click', 'ul.tabs--caption > li:not(.active)', function (i) {
//    var link = $(this);
//    console.log(link.index());
//    $(this).click(function(){
//        $(this).addClass('active').siblings().removeClass('active')
//          .closest('div.tabs').find('.tabs--content-wrap div.tabs__content').removeClass('active').eq(i).addClass('active');
//      });
//
//    if (link.closest('form').hasClass('form-booking')) {
//        switch_booking_step();
//    }
//});
//
//$(document).on('click', 'ul.tabs-inner--caption li', function () {
//    var link = $(this);
//    console.log(link)
//    //$('ul.tabs-inner--caption li').removeClass('active')
//    //link.addClass('active').closest('.tabs-inner').find('.tabs-inner--content').removeClass('active').eq(link.index()).addClass('active');
//
//    if (link.closest('form').hasClass('form-booking')) {
//        switch_booking_step();
//    }
//});

