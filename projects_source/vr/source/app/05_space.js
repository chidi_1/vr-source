// Параллакс
window.onload = function () {
    if (document.getElementById('box') != null) {
        var parallax_box = document.getElementById('box');

        var c1left = document.getElementById('l1').offsetLeft,
            c1top = document.getElementById('l1').offsetTop;


        parallax_box.onmousemove = function (event) {
            event = event || window.event;
            var x = event.clientX - parallax_box.offsetLeft,
                y = event.clientY - parallax_box.offsetTop;

            mouse_parallax('l1', c1left, c1top, x, y, 35);
        }
    }

    if (document.getElementById('boxspace') != null) {
        var parallax_box = document.getElementById('boxspace');

        var s1left = document.getElementById('s1').offsetLeft,
            s1top = document.getElementById('s1').offsetTop,
            s2left = document.getElementById('s2').offsetLeft,
            s2top = document.getElementById('s2').offsetTop,
            s3left = document.getElementById('s3').offsetLeft,
            s3top = document.getElementById('s3').offsetTop,
            s4left = document.getElementById('s4').offsetLeft,
            s4top = document.getElementById('s4').offsetTop,
            s5left = document.getElementById('s5').offsetLeft,
            s5top = document.getElementById('s5').offsetTop,
            s6left = document.getElementById('s6').offsetLeft,
            s6top = document.getElementById('s6').offsetTop,
            s7left = document.getElementById('s7').offsetLeft,
            s7top = document.getElementById('s7').offsetTop,
            s8left = document.getElementById('s8').offsetLeft,
            s8top = document.getElementById('s8').offsetTop,
            s9left = document.getElementById('s9').offsetLeft,
            s9top = document.getElementById('s9').offsetTop;


        parallax_box.onmousemove = function (event) {
            event = event || window.event;
            var x = event.clientX - parallax_box.offsetLeft,
                y = event.clientY - parallax_box.offsetTop;

            mouse_parallax('s1', s1left, s1top, x, y, 35);
            mouse_parallax('s2', s2left, s2top, x, y, 15);
            mouse_parallax('s3', s3left, s3top, x, y, 100);
            mouse_parallax('s4', s4left, s4top, x, y, 45);
            mouse_parallax('s5', s5left, s5top, x, y, 55);
            mouse_parallax('s6', s6left, s6top, x, y, 25);
            mouse_parallax('s7', s7left, s7top, x, y, 75);
            mouse_parallax('s8', s8left, s8top, x, y, 15);
            mouse_parallax('s9', s9left, s9top, x, y, 85);
        }
    }
};

function mouse_parallax(id, left, top, mouseX, mouseY, speed) {
    var obj = document.getElementById(id);
    var parentObj = obj.parentNode,
        containerWidth = parseInt(parentObj.offsetWidth),
        containerHeight = parseInt(parentObj.offsetHeight);
    obj.style.left = left - ( ( ( mouseX - ( parseInt(obj.offsetWidth) / 2 + left ) ) / containerWidth ) * speed ) + 'px';
    obj.style.top = top - ( ( ( mouseY - ( parseInt(obj.offsetHeight) / 2 + top ) ) / containerHeight ) * speed ) + 'px';
}

// фото и видео
function init_space_gallery() {
    var grid = $('.space-content-grid');

    grid.imagesLoaded().progress(function () {
        grid.isotope();
    });

    $('.space-filters li').on('click', function () {
        $(this).addClass('active').siblings().removeClass('active');
        var filterValue = $(this).data('filter');
        grid.isotope({
            filter: filterValue
        });
    });
}