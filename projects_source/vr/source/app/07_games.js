var games_slider;
var games_html;

function init_games_slider() {
    $('.games').addClass('desktop');
    var game_length = $('.games-slider--el').length;
    $('.position-slider').empty();
    var step;

    if (game_length < 10) {
        step = 3
    }
    else {
        if (game_length < 40) {
            step = 5
        }
        else {
            if (game_length < 100) {
                step = 10
            }
            else {
                step = 20
            }
        }
    }

    var position_length = Math.ceil(game_length / step);
    var html = '';

    if (position_length > 3) {
        html = html + '<a href="" class="position-slider--el js--slide-games"  data-step="' + position_length + '">' + (step * (position_length) - (step - 1)) + '-' + game_length + '</a>';
        html = html + '<a href="" class="position-slider--el active js--slide-games"  data-step="' + 1 + '">' + 1 + '-' + step + '</a>';
        html = html + '<a href="" class="position-slider--el js--slide-games"  data-step="' + 2 + '">' + (step + 1) + '-' + (step * 2) + '</a>';

        $('.position-slider-container').removeClass('hidden-block');
        $('.position-slider').html(html);
    }
    else {
        $('.position-slider-container').addClass('hidden-block')
    }

    $('.games-slider--el').each(function () {
        var game = $(this);
        game.css({'background-image': game.data('bgdesktop')});
        var html = '<ul class="game-content--tags">' + game.find('.game-content--tags').html() + '</ul>';
        game.find('.game-content--tags').remove();
        game.find('.game-content--top').append(html);
    });

    if ($('.games-slider--el').length > 3) {

        games_slider = $(".games-slider");
        $('.games-arrows').removeClass('hidden-block');

        games_slider.owlCarousel({
            margin: 0,
            stagePadding: 0,
            loop: true,
            nav: true,
            navText: [,],
            dots: true,
            autoplay: false,
            items: 1,
            onTranslated: function () {
                if (position_length > 3) {
                    set_position();
                }

                setTimeout(function () {
                    disable_control = false;
                }, 500);
            }
        });
    }

    var disable_control = false;

    $(document).on('click', '.js--game-next', function(){
        var active_el_index = $('.games-slider .owl-dots .owl-dot.active').index();
        var step_inner = 3;
        games_slider.trigger('to.owl.carousel', active_el_index + step_inner);

        return false;
    });
    $(document).on('click', '.js--game-prev', function(){
        var active_el_index = $('.games-slider .owl-dots .owl-dot.active').index();
        var step_inner = 3;
        games_slider.trigger('to.owl.carousel', active_el_index - step_inner);
        return false;
    });

    games_slider.on('mousewheel', function (e) {
        var active_el_index = $('.games-slider .owl-dots .owl-dot.active').index();

        if (disable_control == false) {
            disable_control = true;
            need_slide = false;

            if ($(window).width() > 1440) {
                var step_inner = 3
            }
            else {
                if ($(window).width() > 1024) {
                    step_inner = 2
                }
                else {
                    step_inner = 1;
                }
            }

            if (e.deltaY > 0) {
                games_slider.trigger('to.owl.carousel', active_el_index - step_inner);
            } else {
                games_slider.trigger('to.owl.carousel', active_el_index + step_inner);
            }
        }
        e.preventDefault();
    });

    $(document).on('click', '.js--slide-games', function () {
        var index_clicked = Number($(this).data('step'));
        games_slider.trigger('to.owl.carousel', (((index_clicked - 1) * step)));

        return false;
    });

    function set_position() {
        var index = Number(games_slider.find('.owl-item.active').find('.game-number').text());
        var current_position = Math.ceil(index / step);
        var html = '';

        if (index <= step) {
            html = html + '<a href="" class="position-slider--el js--slide-games"  data-step="' + position_length + '">' + (step * (position_length) - (step - 1)) + '-' + game_length + '</a>';
            html = html + '<a href="" class="position-slider--el active js--slide-games"  data-step="' + current_position + '">' + 1 + '-' + step + '</a>';
            html = html + '<a href="" class="position-slider--el js--slide-games"  data-step="' + (current_position + 1) + '">' + (step + 1) + '-' + (step * 2) + '</a>';
        }
        else {
            if (index > game_length - step) {
                html = html + '<a href="" class="position-slider--el js--slide-games"  data-step="' + (current_position - 1) + '">' + (step * (position_length - 1) - (step - 1)) + '-' + (step * (position_length) - step) + '</a>';
                html = html + '<a href="" class="position-slider--el active js--slide-games"  data-step="' + current_position + '">' + (step * (position_length) - (step - 1)) + '-' + game_length + '</a>';
                html = html + '<a href="" class="position-slider--el  js--slide-games"  data-step="' + 1 + '">' + 1 + '-' + step + '</a>';
            }
            else {
                html = html + '<a href="" class="position-slider--el js--slide-games"  data-step="' + (current_position - 1) + '">' + (((current_position - 2) * step) + 1) + '-' + ((current_position - 1) * step) + '</a>';
                html = html + '<a href="" class="position-slider--el active js--slide-games"  data-step="' + current_position + '">' + (((current_position - 1) * step) + 1) + '-' + (step * current_position) + '</a>';
                html = html + '<a href="" class="position-slider--el  js--slide-games"  data-step="' + (current_position + 1) + '">' + (((current_position) * step) + 1) + '-' + ((current_position + 1) * step) + '</a>';
            }
        }

        $('.position-slider').html(html);
    }
}

function init_games_list() {
    $('.games').removeClass('desktop');
    $('.games-slider-container').html(games_html);
    $('.games-arrows').addClass('hidden-block');

    $('.games-slider--el').each(function () {
        var game = $(this);
        game.css({'background-image': game.data('bgmobile')});
        var html = '<ul class="game-content--tags">' + game.find('.game-content--tags').html() + '</ul>';
        game.find('.game-content--tags').remove();
        game.find('.game-content-wrap').append(html);
    })
}

function show_slider_info() {
    $('.games-slider--el').each(function () {
        $(this).find('.slide-wrap').slideDown(0);
        $(this).find('.game-content--btns').fadeIn(0);
    });
}

function hide_slider_info() {
    $('.games-slider--el').each(function () {
        $(this).find('.slide-wrap').slideUp(0);
        $(this).find('.game-content--btns').fadeOut(0);
    });
}

function init_filter() {
    // �������������� �������

    var min = $('.slider-horisontal').data('min');
    var max = $('.slider-horisontal').data('max');

    var slider = document.getElementById('slider-players');

    noUiSlider.create(slider, {
        connect: true,
        step: 1,
        behaviour: 'tap-drag',
        start: [1],
        range: {
            'min': min,
            'max': max
        }
    });

    slider.noUiSlider.on('update.one', function (values) {
        var val = Number(values[0]);
        val = val.toFixed(0);
        $('.inp-players').val(val);
        $('.members-list li').removeClass('active').eq(val - 1).addClass('active');
    });

    $(document).on('click', '.js--change-members', function () {
        var link = $(this);
        link.addClass('active').siblings().removeClass('active');
        slider.noUiSlider.set([link.text()]);
        $('.inp-players').val(link.text());
    });

    // �������� ������
    $(document).on('click', '.js--clear-filter', function () {
        $('.label-checkbox input').removeAttr('checked');
        $('.label-radio input').removeAttr('checked');
        slider.noUiSlider.set(1);
        $('.filter-players .members-list li').removeClass('active');
        $('.filter-players .members-list li:first-child').addClass('active');

        $('.select-styled option').removeAttr('selected');
        $('.select-styled option:first-child').attr('selected', 'selected');

        $('.jq-selectbox__dropdown ul li').removeClass('selected sel');
        $('.jq-selectbox__dropdown ul li:first-child').addClass('selected sel');
        $('.jq-selectbox__select-text').text($('.filter-section .jq-selectbox__dropdown ul li:first-child').text());
        //$('.select-styled .jq-selectbox__dropdown ul li:first-child').trigger('click');
        set_filter();

        return false;
    });
}


function check_games_view() {
    if (is_tablet == false && is_mobile == false) {
        if ($(window).width() < 800) {
            init_games_list();
        }
        else {
            if ($(window).width() <= 1024) {
                if (!$('.games-slider.owl-carousel').length) {
                    init_games_slider(function () {
                        show_slider_info();
                    });
                }
                else {
                    show_slider_info();
                }
            }
            else {
                if (!$('.games-slider.owl-carousel').length) {
                    init_games_slider(function () {
                        hide_slider_info();
                    });
                }
                else {
                    hide_slider_info();
                }
            }
        }
    }
    else {
        if (is_tablet) {
            if (window.orientation == 90) {
                init_games_slider();
            }
            if (window.orientation == 0) {
                init_games_list();
            }
        }
        else {
            init_games_list();
        }
    }
}