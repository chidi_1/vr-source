// слайдер внутренняя страница первый таб
function init_inner_slider() {
    var owl = $('.inner-slider');
    var owl_item;

    var is_tablet = device.tablet();
    var is_mobile = device.mobile();

    var disable_mousewheel = false;

    owl.owlCarousel({
        margin: 0,
        loop: false,
        nav: true,
        dots: true,
        autoplay: false,
        navText: [,],
        items: 1,
        onInitialized: function () {
            $('.inner-slider .owl-dot').each(function () {
                var index = Number($(this).index()) + 1;
                $(this).html('<span>' + index + '</span>');
            });
            owl_item = $('.inner-slider .owl-item .inner-content');
        },
        onTranslate: function () {
            activate_control(1000);
        },
        onTranslated: function () {
            activate_control(1000);
        }
    });

    owl.on('mousewheel', function (e) {

        var active_el_index = $('.inner-slider .owl-dots .owl-dot.active').index();

        if (disable_control == false) {
            disable_control = true;

            if (e.deltaY > 0) {
                owl.trigger('prev.owl');
            } else {
                owl.trigger('next.owl');
            }

            activate_control(1000);
        }
        //activate_control(1000);
        e.preventDefault();
    });


    owl_item.swipe({
        swipe: function (e, direction, distance, duration, fingerCount, fingerData) {

            if (direction == 'up') {
                setTimeout(function () {
                    owl.trigger('next.owl');
                }, 100);
            }
            if (direction == 'down') {
                setTimeout(function () {
                    owl.trigger('prev.owl');
                }, 100);
            }
            if (direction == null) {
                if (is_tablet || is_mobile) {
                    event.target.click();
                }
            }
        },
        threshold: 30
    });
}

// скрыть/показать декоративную страницу
function show_overlay() {
    $('.fullpage').removeClass('hidden');
}

function hide_overlay() {
    $('.fullpage').addClass('hidden');
}

function activate_control(delay) {
    setTimeout(function () {
        disable_control = false;
    }, delay)
}

// скроллбар
function init_scrollbar() {
    $.os = {name: (/(win|mac|linux|sunos|solaris|iphone)/.exec(navigator.platform.toLowerCase()) || [u])[0].replace('sunos', 'solaris')};
    if ($.os.name == 'iphone' && $('window').width() < 360) {
        $('.scrollbar').mCustomScrollbar();
    }
}
