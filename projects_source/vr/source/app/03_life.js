// слайдер фоновых изображений
function init_life_slider() {
    $('.life-slider').owlCarousel({
        margin: 0,
        loop: true,
        nav: true,
        dots: false,
        autoplay: true,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        navText: [,],
        mouseDrag: false,
        touchDrag: false,
        items: 1
    });
}

