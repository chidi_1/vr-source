// отправка даты с календаря
function get_date_data(input) {
    var action = input.data('action');
    var method = input.data('method');
    var data = input.val();

    $.ajax({
        url: action,
        method: method,
        data: {data: data},
        success: function (data) {
            var parse_data = jQuery.parseJSON(data);

            if (input.hasClass('js--load-life')) {
                $('.team-list ul.life-table').empty().html(parse_data.html);
                init_scrollbar();
            }

            if (input.hasClass('js--load-time')) {
                $('.time-list').empty().html(parse_data.html);
                $('.time-total-min').text('0 мин');
                $('.time-total-val').text('0 руб');
            }
        }
    })
}

// показать инфрмацию об игре
function show_game_info(el) {
    var link = el;
    var action = link.data('action');
    var method = link.data('method');
    var data = link.data('id');

    $.ajax({
        url: action,
        method: method,
        data: {'data': data},
        success: function (data) {
            $('.game-info-container').html(data);
            $('.game-info-container').addClass('open');
            $('body').addClass('fixed');
            init_scrollbar();
        }
    });
}

// фильтр

function set_filter(close) {
    var form = $('.filter-form');
    var action = form.attr('action');
    var method = form.attr('method');
    var data = form.serialize();

    $.ajax({
        url: action,
        method: method,
        data: data,
        success: function (data) {
            games_html = data;
            $('.games-arrows').addClass('hidden-block');
            $('.games-slider-container').html(games_html);
            check_games_view();
            if ($('.games-slider').height() < $('.game-filter').height()) {
                $('.games-slider-container').addClass('show-center')
            }
            else {
                $('.games-slider-container').removeClass('show-center')
            }

            if (close == 'true') {
                $('.close-filter.js--close-block').trigger('click');
            }
        }
    })
}

// ищем инфрмацию об игре
function search_games() {
    var form = $('.search-form');
    var action = form.prop('action');
    var method = form.prop('method');
    var data = form.serialize();

    $.ajax({
        url: action,
        method: method,
        data: data,
        success: function (data) {
            games_html = data;
            $('.games-arrows').addClass('hidden-block');
            $('.games-slider-container').empty().html(games_html);
            check_games_view();
            if ($('.games-slider').height() < $('.game-filter').height()) {
                $('.games-slider-container').addClass('show-center')
            }
            else {
                $('.games-slider-container').removeClass('show-center')
            }
        }
    });
}